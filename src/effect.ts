export default class Effect<A> {
  #eff: () => Promise<A>;
  #toString: () => String;

  private constructor(eff: () => Promise<A>, toString: () => String = () => "Effect {}") {
    this.#eff = eff;
    this.#toString = toString;
  }

  execute(): Promise<A> {
    return this.#eff();
  }

  toString(): String {
    return this.#toString();
  }

  map<B>(f: (value: A) => B): Effect<B> {
    return new Effect(
      () => this.execute().then(f),
      () => `Effect.Map {${this.toString()}, ${f}}`
    );
  }

  flatMap<B>(f: (value: A) => Effect<B>): Effect<B> {
    return new Effect(
      () => this.execute().then(value => f(value).execute()),
      () => `Effect.FlatMap {${this.toString()}, ${f}}`
    );
  }

  static pure<T>(value: T | PromiseLike<T>): Effect<T> {
    const resolved = Promise.resolve(value);
    return new Effect(
      () => resolved,
      () => `Effect.Pure {${value}}`
    );
  }

  static lazy<T>(f: () => (T | PromiseLike<T>)): Effect<T> {
    return new Effect(
      () => Promise.resolve().then(() => f()),
      () => `Effect.Lazy {${f}}`
    );
  }
};