import Effect from './effect'
import { expect } from 'chai';

describe("Effect", () => {
  it("allows creating an effect from pure value", async () => {
    const value = {};
    const eff = Effect.pure(value);

    const result = await eff.execute();

    expect(result).to.be.equal(value);
  });

  it("allows creating an effect from pure promise value", async () => {
    const value = {};
    const eff = Effect.pure(Promise.resolve(value));

    const result = await eff.execute();

    expect(result).to.be.equal(value);
  });

  it("caches pure value execution", async () => {
    const value = {};
    const eff = Effect.pure(value);

    const result1 = eff.execute();
    const result2 = eff.execute();

    expect(result1).to.be.equal(result2);
    expect(await result1).to.be.equal(await result2);
  });

  it("caches pure promise value execution", async () => {
    const value = {};
    const eff = Effect.pure(Promise.resolve(value));

    const result1 = eff.execute();
    const result2 = eff.execute();

    expect(result1).to.be.equal(result2);
    expect(await result1).to.be.equal(await result2);
  });


  it("allows lazy computations", async () => {
    let executed = false;
    const calculator = async () => {
      executed = true;
      return 42;
    }

    const eff = Effect.lazy(calculator);

    expect(executed).to.be.equal(false);

    const result = await eff.execute();

    expect(result).to.be.equal(42);
    expect(executed).to.be.equal(true);
  });


  it("allows lazy promise computations", async () => {
    let executed = false;
    const calculator = async () => {
      executed = true;
      return Promise.resolve(42);
    };

    const eff = Effect.lazy(calculator);

    expect(executed).to.be.equal(false);

    const result = await eff.execute();

    expect(result).to.be.equal(42);
    expect(executed).to.be.equal(true);
  });

  it("lazy value execution is not cached", async () => {
    const value = 42;
    const eff = Effect.lazy(() => value);

    const result1 = eff.execute();
    const result2 = eff.execute();

    expect(result1).to.be.not.equal(result2);
  });

  it("lazy promise value execution is not cached", async () => {
    const eff = Effect.lazy(() => Promise.resolve(42));

    const result1 = eff.execute();
    const result2 = eff.execute();

    expect(result1).to.be.not.equal(result2);
  });

  it("lazy value result is not cached", async () => {
    const eff = Effect.lazy(() => ({}));

    const result1 = await eff.execute();
    const result2 = await eff.execute();

    expect(result1).to.be.not.equal(result2);
  });

  it("lazy promise value result is not cached", async () => {
    const eff = Effect.lazy(() => Promise.resolve({}));

    const result1 = await eff.execute();
    const result2 = await eff.execute();

    expect(result1).to.be.not.equal(result2);
  });

  it("has Functor's map", async () => {
    const eff1 = Effect.pure(42)
    const eff2 = eff1.map(a => a + 1);

    const result = await eff2.execute();

    expect(result).to.be.equal(43);
  });

  it("has Monad's flatMap", async () => {
    const eff1 = Effect.pure(42);
    const eff2 = eff1.flatMap((a) => Effect.pure(a + 1));

    const result = await eff2.execute();

    expect(result).to.be.equal(43);
  });
})
